<!DOCTYPE html>
<html lang="en">
  
<head>


<img src="Img\Ins_logo.jpg" alt="" id="logo" style="width: 15%;"><button style="margin-left: 79%;" class="btn btn-primary"><a style="color: white;" href="https://localhost/ins/soap/formEmision.php">Emision</a> </button>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/custom.css">
    <title>Ins</title>
    
</head>
<body>

<div id="contenedor">
<h1 style="text-align: center;">Cotización</h1>
<div id="tabla1">
<table border="1" class="table table-bordered">


<form action="/action_page.php" style="text-align: center;">
<tr>
<th colspan="2">Datos de Usuario:</th>
</tr>
<tr>
<td>
  <label class="" for="fname"> Empresa:</label>
</td>
<td>
  <input class="form-control" type="text" id="id_empresa" name="id_empresa" value="">
</td>
</tr>
<tr>
  <td><label for="lname">Sistema:</label></td>
  <td><input  class="form-control" type="text" id="id_sistema" name="id_sistema" value="" ></td>
  </tr>
  <tr>
    <td>
  <label for="lname">Direcciónes:</label>
  </td>
  <td>
  <input class="form-control" type="text" id="id_direcciones" name="id_direcciones" value=""></td>
  </tr>
  <tr>
    <td>
  <label for="lname">Fecha:</label>
    </td>
    <td>
  <input class="form-control" type="date" id="id_fecha" name="id_fecha" value="">
    </td>
  </tr>
  <tr>
  <td><label for="lcedula">Cedula:</label></td>
  <td>
  <input class="form-control" type="text" id="id_cedula" name="id_cedula" placeholder="" value="">
  </td>
</tr>
  <tr>
    <td>
  <label for="lname">Nombre:</label>
    </td>
    <td>
  <input class="form-control" type="text" id="id_nombre" name="id_nombre" value="">
    </td>
  </tr>
  <tr>
    <td>
  <label for="lname">Primer Apellido:</label>
    </td>
    <td>
  <input class="form-control" type="text" id="id_primerA" name="id_primerA" value="">
    </td>
  </tr>
  <tr>
    <td> 
  <label for="lname">Segundo Apellido:</label>
    </td>
    <td>
  <input class="form-control" type="text" id="id_segundoA" name="id_segundoA" value="">
    </td>
  </tr>
  <tr> 
    <td>
  <label for="edad">Edad:</label>
    </td>
    <td>
  <input class="form-control" type="number" id="female" name="gender" value="F">
    </td>
  </tr>
</div>

  
  <tr>
    <td>
  <label for="lname">Genero:</label>
    </td>
    <td>
  <input type="radio" id="male" name="gender" value="M">
   
  <label for="male">M</label>
    
  <input type="radio" id="female" name="gender" value="F">
    
  <label style="margin-right: 6px;" for="female">F</label>
    </td>
  </tr>
  </div>

    <tr> 
      <td>
  <label for="tipo_seguro">Tipo de Seguro:</label>
      </td>
      <td>
<select class="form-control valid" name="tipo_seguro" id="tipo_seguro">
<option  class="dropdown-item" value="0">-Seleccione una opcion-</option>
  <option  class="dropdown-item" value="1">Asistencia en Carretera</option>
  <option class="dropdown-item" value="2">INS Medical</option>
  <option class="dropdown-item" value="3">Responsabilidad Civil Extracontractual por Daños a la Propiedad de Terceros</option>
  <option class="dropdown-item" value="4">Seguro contra Incendio</option>
  <option class="dropdown-item" value="5">Seguro contra Robo</option>
  <option class="dropdown-item" value="6">Seguro de Accidentes para Estudiantes</option><!-- subtipos--->
  <option class="dropdown-item" value="7">Seguro de Accidentes para Universitarios</option>
  <option class="dropdown-item" value="8">Seguro de Fidelidad Individual</option>
  <option class="dropdown-item" value="9">Seguro de Gastos Médicos</option><!-- subtipos--->
  <option class="dropdown-item" value="10">Seguro Más Proteccion</option>
  <option class="dropdown-item" value="11">Seguro Obligatorio de Automóviles (SOA)</option>
  <option class="dropdown-item" value="12">Seguro Su Vida</option><!-- subtipos--->
  <option class="dropdown-item" value="13">Seguro Tarjeta Segura Individual</option>
  <option class="dropdown-item" value="14">Seguro Voluntario de Automóviles</option>
  <option class="dropdown-item" value="15">Seguros de Salud</option><!-- subtipos--->
  <option class="dropdown-item" value="16">Seguros de Vida</option>
  <option class="dropdown-item" value="17">Seguros para Viajeros</option>
</select>
      </td>
    </tr>
</form>
</table>
</div>
<div id="tabla2">
<table border="1" class="table table-bordered">
<!---Catalogo 909-->
<tr>
<th colspan="2">Datos de Beneficiario:</th>
</tr>
<tr>
  <td>
<label for="tipo_seguro">Parentezco:</label>
  </td>
  <td>
<select class="form-control valid" name="pararentezco" id="parentezco">
<option   value="0">-Seleccione una opcion-</option>
 
  <option class="dropdown-item" value="010">ASEGURADO</option><!-- subtipos--->
  <option class="dropdown-item" value="020">CONYUGE</option>
  <option class="dropdown-item" value="080">CONYUGE VC</option>
  <option class="dropdown-item" value="020">CÓNYUGE</option>
  <option class="dropdown-item" value="060">HERMANOS</option><!-- subtipos--->
  <option class="dropdown-item" value="120">HERMANOS VC</option>
  <option class="dropdown-item" value="030">HIJOS</option>
  <option class="dropdown-item" value="090">HIJOS VC</option><!-- subtipos--->
  <option class="dropdown-item" value="040">MADRE</option>
  <option class="dropdown-item" value="100">MADRE VC</option>
  <option class="dropdown-item" value="000">NO APLICA</option>
  <option class="dropdown-item" value="999">OTROS</option>
  <option class="dropdown-item" value="140">OTROS VC</option>
  <option class="dropdown-item" value="050">PADRE</option><!-- subtipos--->
  <option class="dropdown-item" value="110">PADRE VC</option><!-- subtipos--->
  <option class="dropdown-item" value="070">PATRONO VC</option>
  <option class="dropdown-item" value="130">PATRONO</option>
</select>
  </td>
</tr>
<tr>
  <td><label for="lcedula">Cedula:</label></td>
  <td>
  <input class="form-control" type="text" id="id_cedula" name="id_cedula" placeholder="" value="">
  </td>
</tr>
<tr>
    <td>
  <label for="lname">Nombre:</label>
    </td>
    <td>
  <input class="form-control" type="text" id="id_nombre" name="id_nombre" value="">
    </td>
  </tr>
  <tr>
    <td>
  <label for="lname">Primer Apellido:</label>
    </td>
    <td>
  <input class="form-control" type="text" id="id_primerA" name="id_primerA" value="">
    </td>
  </tr>
  <tr>
    <td> 
  <label for="lname">Segundo Apellido:</label>
    </td>
    <td>
  <input class="form-control" type="text" id="id_segundoA" name="id_segundoA" value="">
    </td>
  </tr>
  <tr> 
    <td>
  <label for="edad">Edad:</label>
    </td>
    <td>
  <input class="form-control" type="number" id="female" name="gender" value="F">
    </td>
  </tr>
</table>
</div>
</div>

<div>
  <tr>
    <td>
  <button style="margin-top: 64%;margin-left: -34%;" class="btn btn-primary" type="submit" onclick="window.location.href='https://localhost/ins/soap/ejemplo.php'" >Consultar</button>
    </td>
  </tr>
  </div>
</body>
</html>