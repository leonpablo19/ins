<?php 


$string = <<<XML
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cot="http://www.ins-cr.com/servicios/transacciones/B2B/cotizacion/cotins" xmlns:tran="http://www.ins-cr.com/servicios/transacciones/B2B/cotizacion/cotins">
   <soapenv:Header>
      <cot:SeguridadHeaderElement>
         <Empresa>C58BD9DA08ED73313052C9AA0E0600BF</Empresa>
         <Sistema>74F43BC2D618E28DD35CC8C9CA5B0AF4</Sistema>
         <Direcciones>FC51F48AFE538353043F38732F53D30F</Direcciones>
      </cot:SeguridadHeaderElement>
   </soapenv:Header>
   <soapenv:Body>
      <cot:CotizarPolizaRequest>
         <InfoSolicitud>
            <InfoTransaccion>
               <FechaHora>2019-04-11T13:49:53</FechaHora>
            </InfoTransaccion>
            <Cotizacion>
               <ConsecutivoConfiguracion>905</ConsecutivoConfiguracion>
               <Solicitante>UsrDummy</Solicitante>
               <Parametros><![CDATA[<PARAMETROS>
	<TIPOIDCLI>0</TIPOIDCLI>
	<IDENTCLI>801170740</IDENTCLI>
	<PRIAPELLIDOCLI>FUENTES</PRIAPELLIDOCLI>
	<SEGAPELLIDOCLI>MUÑIZ</SEGAPELLIDOCLI>
	<PRINOMBRECLI>DAYSI</PRINOMBRECLI>
	<SEGNOMBRECLI>MARGARITA</SEGNOMBRECLI>
	<NOMCOMPLETOCLI>FUENTES MUÑIZ DAYSI MARGARITA</NOMCOMPLETOCLI>
	<GENEROCLI>M</GENEROCLI>
	<FECNACICLI>12/04/1982</FECNACICLI>
	<TIPOTELCLI>02</TIPOTELCLI>
	<NUMTELCLI>88888888</NUMTELCLI>
	<PROVCLI>1</PROVCLI>
	<CANTCLI>01</CANTCLI>
	<DISTCLI>01</DISTCLI>
	<CORREOE>mfuentes@insservicios.com,day.fuentes12@gmail.com</CORREOE>
	<VIGDESDE>25/03/2019</VIGDESDE>
	<VIGHASTA>30/04/2019</VIGHASTA>
	
	<SUCEMI>01</SUCEMI>
	<AGENTE>1106550</AGENTE>
	<FORMAPAGO>A</FORMAPAGO>
	<DIRECRSGO>Casa</DIRECRSGO>
	<MONTO2>0</MONTO2>

	<OBSERVACIONES>PANAMA</OBSERVACIONES>
	<MONTO1>0</MONTO1>
	<CBEDAD>36</CBEDAD>
	<TIPOACTRSGO>0025</TIPOACTRSGO>
	<MOASR>NO</MOASR>
	<TIPORIESGOCOB>01</TIPORIESGOCOB>
	<TIPOTARIFACOB>0001</TIPOTARIFACOB>
	<SUMASEGRSGO>1</SUMASEGRSGO>
	<PHOSINBUC>88888888</PHOSINBUC>

	<TIPOIDBENEF>0</TIPOIDBENEF>
	<IDBENEF>106540419</IDBENEF>
	<IDBENEF></IDBENEF>
	<IDBENEF>MILTON</IDBENEF>

	<PORCENTAJEBENEF>0</PORCENTAJEBENEF>
	<NOMBENEF>020</NOMBENEF>
	
	<PRIAPEBENEF></PRIAPEBENEF>
	<PRIAPEBENEF>NUÑEZ</PRIAPEBENEF>
	<SEGAPEBENEF></SEGAPEBENEF>
	<SEGAPEBENEF>0</SEGAPEBENEF>
	<TIPOIDBENEF></TIPOIDBENEF>
	<IDBENEF></IDBENEF>

	<IDBENEF></IDBENEF>
	<NOMCOMPBENEF></NOMCOMPBENEF>
	<PARENTESCOBENEF></PARENTESCOBENEF>
	<PORCENTAJEBENEF></PORCENTAJEBENEF>
	<NOMBENEF></NOMBENEF>
	
	
	<PRIAPEBENEF></PRIAPEBENEF>
	
	<SEGAPEBENEF></SEGAPEBENEF>
	<TIPOIDBENEF></TIPOIDBENEF>
	<IDBENEF></IDBENEF>

	<IDBENEF></IDBENEF>
	
	<PARENTESCOBENEF></PARENTESCOBENEF>
	<PORCENTAJEBENEF></PORCENTAJEBENEF>
	<NOMBENEF></NOMBENEF>
	
	<PRIAPEBENEF></PRIAPEBENEF>

	<SEGAPEBENEF></SEGAPEBENEF>
	<TIPOIDBENEF></TIPOIDBENEF>
	<IDBENEF></IDBENEF>
	
	<IDBENEF></IDBENEF>
	
	<PORCENTAJEBENEF></PORCENTAJEBENEF>
	<NOMBENEF></NOMBENEF>
	
	<PRIAPEBENEF></PRIAPEBENEF>
	<SEGAPEBENEF></SEGAPEBENEF>
	<SEGAPEBENEF></SEGAPEBENEF>
</PARAMETROS>]]>
			</Parametros>
            </Cotizacion>
         </InfoSolicitud>
      </cot:CotizarPolizaRequest>
   </soapenv:Body>
</soapenv:Envelope>
XML;


$xml = new SimpleXMLElement($string);

$client = new SoapClient('cotins_1.wsdl');
$xml1 = $xml->asXML();

$result = $client->CotizarPoliza($xml1);


$CodEstado = $result->InfoRespuesta->CodEstado;
$MensajeError = $result->InfoRespuesta->MensajeError;

if($CodEstado!=00){
$t="";
$t.='<table border="1px" class="" cellspacing="0" width="100%" height="60"><tbody><tr style="white-space:nowrap"><h2 style="text-align:center">Respuesta cotización</h2></td><td align="center">SOAP</td></tr></tbody></table>';
$t.= '<table class="" border=1px width="100%" cellspacing=0 style="text-align:center">';
$t.='<tr>';
$t.='<th>Codigo de Estado</th>';
$t.='<th>'.$CodEstado.'</th>';
$t.='</tr>';
$t.='<tr>';
$t.='<th>Mensaje</th>';
$t.='<th>'.$MensajeError.'</th>';
$t.='</tr>';
$t.='</table>';
$t.='</table>';
print_r($t);
}else{
	echo 'adios';
}
?>