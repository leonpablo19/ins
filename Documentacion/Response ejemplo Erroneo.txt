﻿<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <NS1:CotizarPolizaResponse xmlns:NS1="http://www.ins-cr.com/servicios/transacciones/B2B/cotizacion/cotins">
         <InfoRespuesta>
            <CodEstado>97</CodEstado>
            <MensajeError>Ha ocurrido un error.</MensajeError>
            <MensajesUsuario>
               <Mensaje>No se encontró ningún paquete asociado al consecutivo de configuración [91].</Mensaje>
            </MensajesUsuario>
            <NumCotizacion/>
            <Moneda/>
            <Observaciones/>
            <DetalleCotizacion/>
         </InfoRespuesta>
      </NS1:CotizarPolizaResponse>
   </soapenv:Body>
</soapenv:Envelope>